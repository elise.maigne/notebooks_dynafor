---
title: "Les autres notebooks"
subtitle: "(y'a pas que Rmarkdown)"  
author: 
  - "Elise Maigné"
output:
  xaringan::moon_reader:
    self_contained: true
    lib_dir: libs
    css: ["css/xaringan-themer.css" , "css/mycss.css"]
    seal: false
    nature:
      highlightStyle: github
      highlightLines: true
      countIncrementalSlides: false
      ratio: "4:3"
---
class: inverse, center, middle
background-image: url(images/leaf_carre_blanc.jpg)
background-size: cover

```{r setup, include=FALSE}
options(htmltools.dir.version = FALSE)
```

```{r xaringan-themer, include=FALSE, warning=FALSE}
library(xaringanthemer)
style_duo_accent(
  primary_color = "#488A78",
  secondary_color = "#eee8d5",
  inverse_background_color = "#488A78"
)
```

# Les autres notebook
### Il ny a pas que Rmarkdown !
.large[Elise Maigné | 3 Dec. 2020]

<img src="images/logo-inrae_inra_logo.png" width="300px"/>

<!-- ############################################################ -->
---

#### Différence Notebook / Document computationnel

<hr>

### Python & autres

.pull-left[
- IPython Notebook --> **Jupyter Notebook**
- plus de 40 langages
- JupyterLab (2018)
]
.pull-right[
- [Démo](https://jupyter.org/try)
]

<hr>

### Matlab 

.pull-left[
- MATLAB Live Editor (~word like)
- widgets ~ applications
]
.pull-right[
- [Démo](https://fr.mathworks.com/products/matlab/live-editor.html#) 
- [Gallery](https://fr.mathworks.com/products/matlab/live-script-gallery.html)
]

<hr>

### Stata

.pull-left[
- markStat
]
.pull-right[
- [Lien](https://data.princeton.edu/stata/markdown)
]





